/**
 * For starting - create car with consumption:
 * var car = Car(5, 0.2, 300);
 *
 * Car module. Created car with custom consumption.
 * @param consumption {number}
 * @param carOil {number} - from 0.1% to 0.3%
 * @param maxSpeed {number}
 * @return {{
 *            start: start,
 *             riding: riding,
 *            checkGas: checkGas,
 *            checkOil: checkOil,
 *           refueling: refueling,
 *           refuelingOil: refuelingOil
 *           }}
 * @constructor
 */
function Car(consumption, carOil, maxSpeed) {

  if (!_isNumeric(consumption) || consumption > 40 || consumption <= 0) {
    _showMessage('Wrong consumption, create new car', 'error');
    return;
  }

  if (!_isNumeric(carOil) || carOil > 0.3 || carOil <= 0) {
    _showMessage('Wrong lube, create new car', 'error');
    return;
  }

  if (!_isNumeric(maxSpeed) || maxSpeed > 500 || maxSpeed <= 0) {
    _showMessage('Wrong maxSpeed, create new car', 'error');
    return;
  }

  let gasBalance = 3;
  let gasConsumption = consumption / 100; //расход топлива за 1 км
  let gasResidue; // Остаток топлива на текущий момент
  let gasForRide; //Сколько понадобится бензина для поездки на заданную дистанцию
  let gasVolume = 35; // объем бака

  let oilBalance = 1000; // Сколько масла в баке при создании
  let lubeConsPrecent = carOil; // Расход масла в процентах
  let oilResidue; // Остаток масла на текущий момент
  let oilVolume = 3000; // объем бака для масла

  let ignition = false; // Зажигание
  let ready = false; // Готовность к поездке

  /**
   * Check gas amount after riding.
   * @param distance {number} - Riding distance.
   * @return {number} - Gas amount.
   */
  function _gasCheck(distance) {
    if (gasBalance <= 0) {
      return 0;
    }

    gasForRide = Math.round(distance * gasConsumption);
    return gasBalance - gasForRide;
  }

  /**
   * Check oil amount after riding.
   * @param distance {number} - Riding distance.
   * @return {number} - Oil amount.
   */
  function _oilCheck(distance) {
    if (oilBalance <= 0) {
      return 0;
    }

    let oilForRide = Math.round(gasForRide * lubeConsPrecent / 100 * 1000);
    return oilBalance - oilForRide;
  }

  /**
   * Show message for a user in the console.
   * @param message {string} - Text message for user.
   * @param type {string} - Type of the message (error, warning, log). log by default.
   */
  function _showMessage(message, type) {
    let messageText = message ? message : 'Error in program. Please call - 066 083 07 06';

    switch (type) {
      case 'error':
        console.error(messageText);
        break;
      case 'warning':
        console.warn(messageText);
        break;
      case 'log':
        console.log(messageText);
        break;
      default:
        console.log(messageText);
        break;
    }
  }

  /**
   * Check car for ride.
   * @param distance {number} - Ride distance.
   */
  function _checkRide(distance) {
    gasResidue = _gasCheck(distance);
    oilResidue = _oilCheck(distance);
  }

  /**
   * Check value to number.
   * @param n {void} - value.
   * @return {boolean} - Is number.
   */
  function _isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  /**
   * Public methods object.
   */
  return {
    /**
     * Start car.
     */
    start: function() {
      ignition = true;

      if (gasBalance <= 0) {
        _showMessage('You don\'t have gas. Please refuel the car.', 'error');
        ready = false;
        return;
      }

      if (oilBalance <= 0) {
        _showMessage('You don\'t have machine oil. Please fill in machine oil.', 'error');
        ready = false;
        return;
      }

      ready = true;

      _showMessage('Ingition', 'log');
    },

    /**
     * Riding function.
     * @param distance {number} - Riding distance.
     * @param speed {number} - Riding speed.
     */
    riding: function(distance, speed) {
      if (!_isNumeric(distance) || distance < 0) {
        _showMessage('Wrong distance', 'error');
        return;
      }

      if (!_isNumeric(speed) || speed < 0 || speed > maxSpeed) {
        _showMessage('Wrong distance', 'error');
        return;
      }

      if (!ignition) {
        _showMessage('You need start car', 'error');
        return;
      }

      if (!ready) {
        ignition = false;
        switch (true) {
          case (gasBalance <= 0):
            _showMessage('You need gas station', 'error');
            break;
          case (oilBalance <= 0):
            _showMessage('You need engine oil', 'error');
            break;
        }

        return;
      }

      _checkRide(distance);

      let gasDriven = Math.round(gasBalance * gasConsumption * 100);

      if (gasResidue <= 0) {
        let distanceLeft = Math.round(distance - gasDriven);

        gasBalance = 0;

        let neddedGas = Math.round(distanceLeft * gasConsumption);
        let neededTime = Math.round(gasDriven / speed * 60);
        _showMessage('Gas over. You have driven - ' + gasDriven + 'km. You need ' + neddedGas + ' liters. ' + distanceLeft + 'km left', 'warning');
        _showMessage('Time at a speed of ' + speed + ' kilometers per hour and a distance of ' + gasDriven + ' kilometers is ' + neededTime + ' minutes', 'warning');
      }

      if (gasResidue > 0) {
        let neededTime = Math.round(distance / speed * 60);
        gasBalance = gasResidue;
        oilBalance = oilResidue;
        _showMessage('You arrived. Gas balance - ' + gasBalance + ' l' + '. Oil balance - ' + oilBalance + ' g', 'log');
        _showMessage('Time at a speed of ' + speed + ' kilometers per hour and a distance of ' + distance + ' kilometers is ' + neededTime + ' minutes', 'warning');
      }
    },

    /**
     * Check gas function.
     */
    checkGas: function() {
      _showMessage('Gas - ' + gasBalance + ' l.', 'log');
    },

    /**
     * Check oil function.
     */
    checkOil: function() {
      _showMessage('Oil - ' + oilBalance + ' g.', 'log');
    },

    /**
     * Refueling function.
     * @param gas
     */
    refueling: function(gas) {
      if (!_isNumeric(gas) || gas < 0) {
        _showMessage('Wrong gas amount', 'error');
        return;
      }

      if (gasVolume === gasBalance) {
        _showMessage('Gasoline tank is full', 'warning');
      } else if (gasVolume < gasBalance + gas) {
        gasBalance = 35;
        let excess = Math.round(gas - gasBalance);
        _showMessage('Gasoline tank is full. Excess - ' + excess + ' l.', 'log');
      } else {
        gasBalance = Math.round(gasBalance + gas);
        _showMessage('Gas balance - ' + gasBalance, 'log');
      }
    },

    /**
     * Refueling Oil function.
     * @param oil {number}
     */
    refuelingOil: function(oil) {
      if (!_isNumeric(oil) || oil < 0) {
        _showMessage('Wrong oil amount', 'error');
        return;
      }

      if (oilVolume === oilBalance) {
        _showMessage('Oil tank is full', 'warning');
      } else if (oilVolume < oilBalance + oil) {
        oilBalance = 3000;
        let excessOil = Math.round(oil - oilBalance);
        _showMessage('Oil tank is full. Excess - ' + excessOil + ' g.', 'log');
      } else {
        oilBalance = Math.round(oilBalance + oil);
        _showMessage('Oil balance - ' + oilBalance + ' g.', 'log');
      }
    },
  };
}

let car_1 = new Car(7, 0.2, 100);
car_1.start();
car_1.refueling(100);
car_1.riding(200, 70);

let car_2 = new Car(10, 0.3, 200);
car_2.start();
car_2.refueling(100);
car_2.riding(30, 50);

let car_3 = new Car(10, 0.3, 200);
car_3.start();
car_3.refueling(100);
car_3.riding(200, 20);





